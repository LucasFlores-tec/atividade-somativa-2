import React, { Component } from "react";
import { Link } from "react-router-dom";
import fb from "../../Firebase";
import "./index.css";

class Acesso extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            senha: "",
            failedMensagem: ""
        }

        this.criar = this.criar.bind(this);
        this.autenticar = this.autenticar.bind(this);
    }

    criar = evento => {
        const { name, value } = evento.target;
        this.setState({...this.state, [ name ]: value });
    }

    autenticar = async () => {
        const { email, senha } = this.state;

        await fb.auth().signInWithEmailAndPassword( email, senha )
        .then(() => { window.location.href = "./principal" })
        .catch(() => { this.setState({ failedMensagem: "Usuário ou senha incorreto!" }) });
        
        this.setState({ email: "", senha: "" });
    }

    render() {
        return (
            <div>
                <div className="home">
                    <div className="home-title">
                        <h1> HOME </h1>
                    </div>
                    <div className="home-error">
                        <p>{ this.state.failedMensagem }</p>
                    </div>
                    <div className="home-login-inputs">
                        <div>
                            <input 
                                type="email" 
                                name="email" 
                                placeholder="Email" 
                                value={ this.state.email }
                                onChange={ this.criar } />
                        </div>
                        <div>
                            <input 
                                type="password" 
                                name="senha" 
                                placeholder="Senha" 
                                value={ this.state.senha }
                                onChange={ this.criar } />
                        </div>
                    </div>
                    <div className="home-autenticar">
                        <button onClick={ this.autenticar }> Acessar </button>
                    </div>
                    <div className="home-menu-navegacao">
                        <div>
                            <p>Ainda não possui acesso? <Link to="/cadastro"><strong>Cadastre-se!</strong></Link></p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Acesso;