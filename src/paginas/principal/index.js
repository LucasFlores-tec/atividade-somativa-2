import React, { Component } from "react";
import { Link } from "react-router-dom";
import fb from "../../Firebase";
import "./index.css";

class Principal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            nome: "",
            sobrenome: "",
            dataNascimento: ""
        }
    }

    componentDidMount = async () => {
        await fb.auth().onAuthStateChanged(
            async (retorno) => {
                if (retorno) {
                    var uid = retorno.uid;
                    await fb.firestore().collection("usuario").doc(uid).get()
                    .then((dados) => {
                        this.setState({
                            nome: dados.data().nome,
                            sobrenome: dados.data().sobrenome,
                            dataNascimento: dados.data().data_nascimento
                        })
                    })
                }
            }
        )
    }

    render() {
        return (
            <div className="principal">
                <div className="principal-title">
                    <h1> Tela Principal </h1>
                </div>
                <div className="principal-dados">
                    <h2>Dados do usuário logado:</h2>
                    <h3> Nome: { this.state.nome } </h3>
                    <h3> Sobrenome: { this.state.sobrenome } </h3>
                    <h3> Data de nascimento: { this.state.dataNascimento } </h3>
                </div>
                <div className="principal-menu-navegacao">
                    <p>voltar para <Link to="/"><strong>Home</strong></Link></p>
                </div>
            </div>
        )
    }
}

export default Principal;