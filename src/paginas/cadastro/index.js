import React, { Component } from "react";
import { Link } from "react-router-dom";
import fb from "../../Firebase";
import "./index.css";

class Cadastro extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            senha: "",
            nome: "",
            sobrenome: "",
            dataNascimento: new Date
        };

        this.criar = this.criar.bind(this);
        this.cadastrar = this.cadastrar.bind(this);
    }

    criar = evento => {
        const { name, value } = evento.target;
        this.setState({ ...this.state, [name]: value });
    }

    cadastrar = async () => {
        const { email, senha, nome, sobrenome, dataNascimento } = this.state;
        await fb.auth().createUserWithEmailAndPassword( email, senha )
        .then( async (resposta) => {
            await fb.firestore().collection("usuario").doc(resposta.user.uid).set({
                nome: nome,
                sobrenome: sobrenome,
                data_nascimento: dataNascimento
            });
        });
        
        this.setState({ email:"", senha:"", nome:"", sobrenome:"", dataNascimento: new Date })
        window.location.href = "./";
    }

    render() {
        return (
            <div>
                <div className="cadastro">
                    <div className="cadastro-title">
                        <h1> Tela de Cadastro </h1>
                    </div>
                    <div className="cadastro-inputs">
                        <div>
                            <input 
                                type="email" 
                                name="email" 
                                placeholder="Email" 
                                value={ this.state.email } 
                                onChange={ this.criar } />
                        </div>
                        <div>
                            <input 
                                type="password" 
                                name="senha" 
                                placeholder="Senha" 
                                value={ this.state.senha } 
                                onChange={ this.criar } />
                        </div>
                        <div>
                            <input 
                                type="text" 
                                name="nome" 
                                placeholder="Nome" 
                                value={ this.state.nome } 
                                onChange={ this.criar } />
                        </div>
                        <div>
                            <input 
                                type="text" 
                                name="sobrenome" 
                                placeholder="Sobrenome" 
                                value={ this.state.sobrenome } 
                                onChange={ this.criar } />
                        </div>
                        <div>
                            <input 
                                type="date" 
                                name="dataNascimento" 
                                placeholder="Data de nascimento" 
                                value={ this.state.dataNascimento } 
                                onChange={ this.criar } />
                        </div>
                    </div>
                </div>
                <div className="cadastro-cadastrar">
                    <button onClick={ this.cadastrar }> Cadastrar </button>
                </div>
                <div className="cadastro-menu-navegacao">
                    <p>voltar para <Link to="/"><strong>Home</strong></Link></p>
                </div>
            </div>
        )
    }
}

export default Cadastro;