import { BrowserRouter, Routes, Route, Link } from "react-router-dom";

import Acesso from "./paginas/acesso";
import Cadastro from "./paginas/cadastro";
import Principal from "./paginas/principal";

const Rotas = () => {
    return (
        <div>
            <BrowserRouter>
                <Routes>
                    <Route exact={ true } path="/" element={ <Acesso/> }/>
                    <Route exact={ true } path="/cadastro" element={ <Cadastro/> }/>
                    <Route exact={ true } path="/principal" element={ <Principal/> }/>
                </Routes>
            </BrowserRouter>
        </div>
    )
}

export default Rotas;