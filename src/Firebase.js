import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyCoMex-vouPOofaLYbC7JxkDw-vhiLTzlY",
    authDomain: "projetoead-772ef.firebaseapp.com",
    projectId: "projetoead-772ef",
    storageBucket: "projetoead-772ef.appspot.com",
    messagingSenderId: "963991073934",
    appId: "1:963991073934:web:b3a142bb7f9b3a360845df"
};

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}

export default firebase;