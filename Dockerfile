FROM ubuntu:latest

WORKDIR /reactapp

COPY . /reactapp

RUN apt-get update

RUN apt-get install -y curl nodejs npm

RUN npm install

EXPOSE 3000

CMD ["npm", "start"]